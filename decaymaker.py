import numpy as np
import uproot_methods
import time
import awkward

import logging
log = logging.getLogger('cola.decaymaker')

from toys import rambo


def generate_momenta(nfinal,system_mass,daughter_masses):
    mylist = rambo.FortranList(1,nfinal)
    for i in range(1,nfinal+1):
        mylist[i] = daughter_masses[i-1]
    p = []
    p_rambo,w = rambo.RAMBO(nfinal, system_mass, mylist)
    # Reorder momenta from px,py,pz,E to E,px,py,pz scheme
    for i in range(1, nfinal+1):
        momi = [p_rambo[(4,i)], p_rambo[(1,i)],
                p_rambo[(2,i)], p_rambo[(3,i)]]
        p.append(momi)
    momenta = np.asarray(p)
    return momenta

def generate_vectors(*args, **kwargs):
    x = generate_momenta(*args, **kwargs)
    v = uproot_methods.TLorentzVectorArray.from_cartesian(x[...,1],x[...,2],x[...,3],x[...,0])
    return v

def decay_particle(vector, nfinal, daughter_masses):
    dv = generate_vectors(nfinal, vector.mass, daughter_masses)
    if vector.beta:
        boosted = [dd.boost((vector.p3 / vector.p) * vector.beta) for dd in dv]
    else:
        boosted = dv
    return boosted


def get_decay(decay,key,particle_info):
    nparts = len(decay[key])
    masses = []
    for d in decay[key]:
        if isinstance(d, str):
            part = d
            m = particle_info[part]['mass']
        if isinstance(d, dict):
            part = list(d.keys())[0]
            m = np.random.normal(particle_info[part]['mass'], particle_info[part]['width'])
        masses.append(m)
    return nparts, masses


def decay_it(vector,decay,key,particle_info):
    nparts, masses = get_decay(decay,key,particle_info)
    v = decay_particle(vector, nparts, masses)
    for i,d in enumerate(decay[key]):
        if isinstance(d, str):
            yield v[i]
        elif isinstance(d, dict):
            k = list(d.keys())[0]
            for vv in decay_it(v[i], d, k, particle_info):
                yield vv


def fixed_gen():
    while True:
        particle_info = {
            'N': {'mass': 300, 'width': 0.1 * 300},
            't': {'mass':  175, 'width': 0.1 * 175}, 
            'b': {'mass':  5, 'width': None}, 
            's': {'mass':  1, 'width': None}, 
            'j': {'mass':  1, 'width': None}, 
            'W': {'mass':  80, 'width': 0.1 * 80.}, 
        }

        decay_tree = {
            'N': [{'t': [{'W': ['j','j']},'b']},'b','s'],
            'C': ['s','b','b'],
            'R': ['j'] * 3
        }
        final_state = ['N']
        yield final_state, decay_tree, particle_info

def decay_event(decayspec,shuffle):
    hard_process_final, DECAY, particle_info = decayspec
    key = list(DECAY.keys())[0]
    v = uproot_methods.TLorentzVector(0, 0, 0, particle_info[key]['mass'])
    e = [y for p in hard_process_final for y in decay_it(v, DECAY, p, particle_info)]

    if shuffle:
        np.random.shuffle(e)

    pt  = [x.pt for x in e]
    eta = [x.eta for x in e]
    phi = [x.phi for x in e]
    e   = [x.t for x in e]
    return pt, eta, phi, e

def make_batch(decayspec_gen=None, N=200, shuffle=True):
    a_pt, a_eta, a_phi, a_e = [],[],[],[]
    decayspec_gen = decayspec_gen or fixed_gen()
    for n in range(N):
        while True:
            try:
                pt, eta, phi, e = decay_event(next(decayspec_gen), shuffle=shuffle)
                break
            except rambo.RAMBOError as e:
                log.debug(e)
                #time.sleep(0.1)
        a_pt.append(pt)
        a_eta.append(eta)
        a_phi.append(phi)
        a_e.append(e)

    pt = awkward.fromiter(a_pt)
    eta = awkward.fromiter(a_eta)
    phi = awkward.fromiter(a_phi)
    e = awkward.fromiter(a_e)

    return awkward.Table(pt=pt, eta=eta, phi=phi, e=e)


def main():
    print(make_batch())

if __name__ == '__main__':
    main()
