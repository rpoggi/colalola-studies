
from pathlib import Path

import uproot
import uproot_methods
import awkward
import numpy as np
import torch


def read_batch(N=500):
    mycache = uproot.cache.ArrayCache(100000*1024) # 100 MB
    data = uproot.lazyarrays(
        '/home/users/p/poggi/scratch/xsttbar_samples/user.rpoggi.period*.physics_Main.DAOD_SUSY4*.TTDIFFXS34_R21_allhad_resolved.root/*.root',
        'nominal',
        ['reco_bjets', 'jet_pt', 'jet_eta', 'jet_phi', 'jet_e', 'jet_n',
         'reco_index_w1ja', 'reco_index_w1jb', 'reco_index_w2ja',
         'reco_index_w2jb', 'reco_index_b1', 'reco_index_b2',
         'reco_Chi2Fitted', 'reco_DRbWMax', 'reco_DRbb', 'reco_t1_m',
         'reco_t2_m'],
        entrysteps=1000,
        cache=mycache
    )
    print('Nr. of events: {}'.format(len(data)))

    jets = {
        'sig': {'pt': [], 'eta': [], 'phi': [], 'e': []},
        'bkg': {'pt': [], 'eta': [], 'phi': [], 'e': []}
    }
    jets['sig'] = jets['bkg']

    for i in range(len(data)):
        if len(jets['sig']['pt']) >= N and len(jets['bkg']['pt']) >= N:
            break

        # base requirement
        if not (data['jet_n'][i] == 6 and
                data['reco_Chi2Fitted'][i] < 10. and
                data['reco_DRbWMax'][i] < 2.2 and
                data['reco_DRbb'][i] > 2.0):
            #print('no basic requirement')
            continue

        bkg = data['reco_bjets'][i] == 0 and \
	      (data['reco_t1_m'][i] < 120000 or data['reco_t1_m'][i] > 250000 or \
               data['reco_t2_m'][i] < 120000 or data['reco_t2_m'][i] > 250000)

        if bkg:
            sig = False
        else:
            sig = data['reco_bjets'][i] == 2 and \
                  data['reco_t1_m'][i] > 130000 and data['reco_t1_m'][i] < 200000 and \
                  data['reco_t2_m'][i] > 130000 and data['reco_t2_m'][i] < 200000

        if not (sig or bkg):
            continue

        if sig:
            tag = 'sig'
        elif bkg:
            tag = 'bkg'

        if len(jets[tag]['pt']) >= N:
            # continue if already have enough
            continue

        pt = []
        eta = []
        phi = []
        e = []
        for name in ['reco_index_w1ja', 'reco_index_w1jb',
                     'reco_index_w2ja', 'reco_index_w2jb',
                     'reco_index_b1', 'reco_index_b2']:
            pt.append(data['jet_pt'][i][data[name][i]])
            eta.append(data['jet_eta'][i][data[name][i]])
            phi.append(data['jet_phi'][i][data[name][i]])
            e.append(data['jet_e'][i][data[name][i]])

        jets[tag]['pt'].append(pt)
        jets[tag]['eta'].append(eta)
        jets[tag]['phi'].append(phi)
        jets[tag]['e'].append(e)

    sig_batch = awkward.Table(
        pt=awkward.fromiter(jets['sig']['pt']),
        eta=awkward.fromiter(jets['sig']['eta']),
        phi=awkward.fromiter(jets['sig']['phi']),
        e=awkward.fromiter(jets['sig']['e'])
    )

    bkg_batch = awkward.Table(
        pt=awkward.fromiter(jets['bkg']['pt']),
        eta=awkward.fromiter(jets['bkg']['eta']),
        phi=awkward.fromiter(jets['bkg']['phi']),
        e=awkward.fromiter(jets['bkg']['e'])
    )

    return sig_batch, bkg_batch


def make_torch_batch(N):
    sig_batch, bkg_batch = read_batch(N=N)
    sig_batch = uproot_methods.TLorentzVectorArray.from_ptetaphi(sig_batch.pt, sig_batch.eta, sig_batch.phi, sig_batch.e)
    sX = np.stack([sig_batch.t.regular(), sig_batch.x.regular(), sig_batch.y.regular(), sig_batch.z.regular()], axis=-1)
    sY = np.ones(len(sX))

    bkg_batch = uproot_methods.TLorentzVectorArray.from_ptetaphi(bkg_batch.pt, bkg_batch.eta, bkg_batch.phi, bkg_batch.e)
    bX = np.stack([bkg_batch.t.regular(), bkg_batch.x.regular(), bkg_batch.y.regular(), bkg_batch.z.regular()], axis=-1)
    bY = 0 * np.ones(len(bX))

    X = np.concatenate([sX, bX])
    Y = np.concatenate([sY, bY])

    X = torch.as_tensor(X, dtype=torch.float)
    Y = torch.as_tensor(Y, dtype=torch.float)

    return X, Y


if __name__ == '__main__':
    print(make_torch_batch(N=2))

    #samples_dir = Path('/home/users/p/poggi/scratch/xsttbar_samples')
    #data_path = samples_dir / 'user.rpoggi.periodA.physics_Main.DAOD_SUSY4.grp16_v01_p3372.TTDIFFXS34_R21_allhad_resolved.root' / 'user.rpoggi.18207181._000001.allhad_resolved.root'
    #mc_path = samples_dir / 'user.rpoggi.410471.PhPy8EG.DAOD_TOPQ1.e6337_e5984_s3126_r9364_r9315_p3629.TTDIFFXS36_R21_allhad_resolved.root' / 'user.rpoggi.18378247._000001.allhad_resolved.root'

    #rfile = uproot.open('/home/users/p/poggi/scratch/xsttbar_samples/user.rpoggi.410428.PhPy8EG.DAOD_TOPQ1.e6735_e5984_s3126_r9364_r9315_p3629.TTDIFFXS36_R21_allhad_resolved.root/user.rpoggi.18378248._000001.allhad_resolved.root')
    #rfile = uproot.open('/home/users/p/poggi/scratch/xsttbar_samples/user.rpoggi.410471.PhPy8EG.DAOD_TOPQ1.e6337_e5984_s3126_r9364_r9315_p3629.TTDIFFXS36_R21_allhad_resolved.root/user.rpoggi.18378247._000001.allhad_resolved.root')


    #rfile = uproot.open(str(data_path))
    #print(rfile['nominal'].show())
    #print(rfile['nominal']['jet_n'].array())

    #nominal = rfile[b'nominal']
    #print(nominal.show())
    #print(nominal.numentries)

    #rfile[b'nominal'][b'reco_b1_isbtagged']
    #rfile[b'nominal'][b'reco_b2_isbtagged']
    #print(nominal[b'reco_index_w1ja'])
    #rfile[b'nominal'][b'reco_index_w1jb']
    #rfile[b'nominal'][b'reco_index_w2ja']
    #rfile[b'nominal'][b'reco_index_w2jb']
    #rfile[b'nominal'][b'reco_index_b1']
    #rfile[b'nominal'][b'reco_index_b2']
    #rfile[b'nominal'][b'reco_index_extrajets']

    #jet_pt = nominal[b'jet_pt'].array()
    #rfile[b'nominal'][b'jet_eta']
    #rfile[b'nominal'][b'jet_phi']
    #rfile[b'nominal'][b'jet_e']

    #reco_index_w1ja = nominal[b'reco_index_w1ja'].array()
    #reco_index_w1jb = nominal[b'reco_index_w1jb'].array()
    #bjet_n_77 = nominal[b'bjet_n_77'].array()
    #bjet_n = nominal[b'bjet_n'].array()
    #jet_n = nominal[b'jet_n'].array()

    #c = 0
    #d = 0
    #for i in range(nominal.numentries):
    #    if jet_n[i] != 6:
    #        continue

    #    if bjet_n_77[i] == 0:
    #        tag = 'bkg'
    #        c+=1
    #    elif bjet_n_77[i] == 2:
    #        tag = 'sig'
    #        d+= 1

    #    w1ja_pt = jet_pt[i][reco_index_w1ja[i]]
    #    w1jb_pt = jet_pt[i][reco_index_w1jb[i]]
        #print(i, tag, bjet_n[i], w1ja_pt, w1jb_pt)

    #print(c, d)
