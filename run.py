from __future__ import print_function

import awkward
import uproot_methods
import matplotlib.pyplot as plt
import numpy as np
import torch

from progressbar import progressbar

from pathlib import Path

import logging
# create logger with 'spam_application'
log = logging.getLogger('cola')
log.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler('cola.log')
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
# create formatter and add it to the handlers
ff= logging.Formatter('[%(asctime)s] %(levelname)s :: %(message)s')
cf = logging.Formatter('[%(levelname)s] %(message)s')
fh.setFormatter(ff)
ch.setFormatter(cf)
# add the handlers to the logger
log.addHandler(fh)
log.addHandler(ch)

import decaymaker


def signaldecay(root, res_one, res_two, res_tre,leafs):
    while True:
        particle_info = {
            '_': {'mass': root, 'width': None},
            'C': {'mass': res_one, 'width': res_one*0.1},
            'T': {'mass': res_two, 'width': res_two*0.1},
            'W': {'mass': res_tre, 'width': res_tre*0.1},
            'J': {'mass': leafs[0], 'width': None},
            'B': {'mass': leafs[1], 'width': None},
            'S': {'mass': leafs[2], 'width': None},
        }

        decay_tree = {
            '_': ['J','J','J','J','J','J','J','J','J','J'],
        }
        final_state = ['_']
        yield final_state, decay_tree, particle_info


def backgrounddecay(root, res_one, res_two, res_tre,leafs):
    while True:
        particle_info = {
            '_': {'mass': root, 'width': None},
            'C': {'mass': res_one, 'width': res_one*0.1},
            'T': {'mass': res_two, 'width': res_two*0.1},
            'W': {'mass': res_tre, 'width': res_tre*0.1},
            'J': {'mass': leafs[0], 'width': None},
            'B': {'mass': leafs[1], 'width': None},
            'S': {'mass': leafs[2], 'width': None},
        }

        decay_tree = {
            '_': [{'C': [{'T': [{'W': ['J','J']},'J']},'J','J']},{'C': [{'T': [{'W': ['J','J']},'J']},'J','J']}],
        }
        final_state = ['_']
        yield final_state, decay_tree, particle_info


def make_torch_batch(N):
    batch = decaymaker.make_batch(signaldecay(1000, 300, 175, 80, [1, 1, 1]), shuffle=True, N=N)
    batch = uproot_methods.TLorentzVectorArray.from_ptetaphi(batch.pt, batch.eta, batch.phi, batch.e)
    sX = np.stack([batch.t.regular(), batch.x.regular(), batch.y.regular(), batch.z.regular()], axis=-1)
    sY = 1 * np.ones(len(sX))

    batch = decaymaker.make_batch(backgrounddecay(1000, 300, 175, 80, [1, 1, 1]), shuffle=True, N=N)
    batch = uproot_methods.TLorentzVectorArray.from_ptetaphi(batch.pt, batch.eta, batch.phi, batch.e)
    bX = np.stack([batch.t.regular(), batch.x.regular(), batch.y.regular(), batch.z.regular()], axis=-1)
    bY = 0 * np.ones(len(bX))

    X = np.concatenate([sX, bX])
    Y = np.concatenate([sY, bY])

    X = torch.as_tensor(X, dtype=torch.float)
    Y = torch.as_tensor(Y, dtype=torch.float)
    return X, Y



if __name__ == '__main__':
    from colalola import CoLaLoLa
    log.info('========= CoLaLoLa main =======')

    Path('data').mkdir(exist_ok=True)

    log.info('Instantiation CoLaLoLa model')
    model = CoLaLoLa(10, 30)
    opt = torch.optim.Adam(model.parameters())
    losses = []

    log.info('Start training')
    for i in progressbar(range(400)):
        model.train()
        opt.zero_grad()

        X, Y = make_torch_batch(500)
        P = model(X)
        P, Y = P, Y.reshape(-1,1)

        loss = torch.nn.functional.binary_cross_entropy(P, Y)
        losses.append(float(loss))
        #if i % 25 == 0:
        #    print(loss)
        #    #plt.plot(losses)
        #    #plt.show()
        loss.backward()
        opt.step()

    fig = plt.figure()
    plt.plot(losses)
    plt.savefig('data/losses.png')

    log.info('Save plot model')
    fig = plt.figure()
    plt.imshow(model.cola.w_combo.data.numpy())
    plt.savefig('data/w_combo.png')

    log.info('Make torch batch')
    X,Y = make_torch_batch(5000)
    P = model(X)
    _P = P.data.numpy()
    _Y = Y.data.numpy()

    log.info('Plot Histograms')
    fig = plt.figure()
    plt.hist(_P[_Y==0], bins = np.linspace(0,1,51), density=True, histtype='step')
    plt.hist(_P[_Y==1], bins = np.linspace(0,1,51), density=True, histtype='step')
    #plt.semilogy()
    plt.savefig('data/score.png')

    w = model.cola.w_combo.data.numpy()

    fig = plt.figure()
    plt.hist(X.data.numpy()[Y==0, :, 1].ravel(), bins=100)

    plt.savefig('data/ravel.png')

    X.data.numpy()[Y==0, :, 1]

    log.info('Make torch batch')
    X, Y = make_torch_batch(5000)
    np.unique(X[Y==1].data.numpy()[:, :, 0].ravel()).shape
    fig = plt.figure()
    _,b,_ = plt.hist(X[Y==1].data.numpy()[:, :, 0].ravel(),bins=100);
    plt.hist(X[Y==0].data.numpy()[:, :, 0].ravel(),bins=b);
    plt.savefig('data/ravel_2.png')
