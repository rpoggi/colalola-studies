#!/bin/bash


echo "[INFO] Setup ATLAS software... (will take a while)"
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh > /dev/null


LCG="LCG_96"
ARCH="x86_64-centos7-gcc8-opt"

echo "[INFO] Setup root 6.18.00-${ARCH}..."
lsetup "root 6.18.00-${ARCH}" > /dev/null

echo "[INFO] Setup python..."
lsetup python > /dev/null
#echo "[INFO] Setup python module Jinja2..."
#lsetup "lcgenv -p ${LCG} ${ARCH} Jinja2" > /dev/null
#echo "[INFO] Setup python module click..."
#lsetup "lcgenv -p ${LCG} ${ARCH} click" > /dev/null
echo "[INFO] Setup python module pathlib2..."
lsetup "lcgenv -p ${LCG} ${ARCH} pathlib2" > /dev/null

echo "[INFO] Finished"
